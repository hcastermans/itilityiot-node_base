FROM hypriot/rpi-iojs:1.4.1
MAINTAINER Henk-Jan Castermans <henk-jan.castermans@itility.nl>

# Adding source files into container
ADD src/ /src

# Define working directory
WORKDIR /src

# Install app dependencies
RUN npm install

# Open Port 80
EXPOSE 80
